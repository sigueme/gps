extern crate bytes;
extern crate futures;
extern crate tokio_io;
extern crate tokio_proto;
extern crate tokio_service;
extern crate time;
extern crate clap;
extern crate reqwest;
extern crate chrono;
extern crate env_logger;

#[macro_use]
extern crate log;

use tokio_proto::TcpServer;
use std::sync::Arc;
use clap::{Arg, App};

mod proto;
mod service;

pub use proto::codec::Pos;

fn main() {
    let matches = App::new("Fleety gps receiver")
        .version("1.0.0")
        .author("Abraham Toriz <categulario@gmail.com>")
        .about("Listens for gps entries on the given port.")
        .arg(Arg::with_name("org")
            .short("o")
            .long("org")
            .value_name("ORGANIZATION")
            .help("The organization this thread belongs to")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("url")
             .short("u")
             .long("url")
             .value_name("URL")
             .help("Used to report locations")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("PORT")
            .help("The port to listen to")
            .takes_value(true))
        .get_matches();

    // Specify the localhost address
    let port = matches.value_of("config").unwrap_or("8502");
    let addr = format!("0.0.0.0:{}", port).parse().unwrap();

    // The builder requires a protocol and an address
    let server = TcpServer::new(proto::LineProto, addr);

    // This GPS listener process is bounded to this organization
    let orgname = Arc::new(matches.value_of("org").expect("Need ORG parameter").to_string());

    let url = Arc::new(matches.value_of("url").expect("The url is required").to_string());

    env_logger::init().expect("Couldn't initialize the logger :(");

    debug!("Now running on port {}", port);
    debug!("Will publish to server on {}", url);

    // Instantiate the service for each connection
    server.serve(move || {
        return Ok(
            service::Echo::new()
                .org(orgname.to_string())
                .url(url.to_string())
        );
    });
}
