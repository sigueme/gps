use tokio_service::Service;
use futures::{future, Future, BoxFuture};
use std::io;
use reqwest;

use Pos;

pub struct Echo {
    org      : String,
    url      : String,
}

impl Echo {
    pub fn new() -> Echo {
        Echo {
            org      : "noorg".to_string(),
            url      : "http://api.getfleety.dev/i6l/<token>/location".to_string(),
        }
    }

    pub fn org(mut self, org: String) -> Echo {
        self.org = org.to_string();
        self
    }

    pub fn url(mut self, url: String) -> Echo {
        self.url = url.to_string();
        self
    }
}

impl Service for Echo {
    // These types must match the corresponding protocol types:
    type Request = Pos;
    type Response = String;

    // For non-streaming protocols, service errors are always io::Error
    type Error = io::Error;

    // The future for computing the response; box it for simplicity.
    type Future = BoxFuture<Self::Response, Self::Error>;

    // Produce a future for computing a response from a request.
    fn call(&self, req: Self::Request) -> Self::Future {
        debug!("{:?}", req);
        let params = [
            ( "org"  , self.org.to_string()   ),
            ( "code" , req.device.to_string() ),
            ( "lon"  , req.lon.to_string()    ),
            ( "lat"  , req.lat.to_string()    ),
            ( "time" , req.time.to_string()   ),
            ( "rcvd" , req.rcvd.to_string()   ),
        ];
        let client = reqwest::Client::new().unwrap();

        let res = client.post(&self.url).unwrap()
            .form(&params).unwrap()
            .send();

        match res {
            Ok(response) => {
                if ! response.status().is_success() {
                    error!("Got server error: {:?}", response.status());
                } else {
                    debug!("Sent to server");
                }

                future::ok("".to_string()).boxed()
            },
            Err(_) => future::ok("".to_string()).boxed(),
        }
    }
}
