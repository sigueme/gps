#[derive(Debug)]
pub struct Device {
    id            : String,
    code          : String,
    created_at    : i64,
    last_pos_time : i64,
}
