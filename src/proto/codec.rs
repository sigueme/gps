use std::io;
use std::str;
use bytes::BytesMut;
use tokio_io::codec::{Encoder, Decoder};
use std::str::FromStr;
use time::get_time;
use chrono::Utc;
use chrono::TimeZone;

fn extract_coord(num: f32, orientation: &str) -> f32 {
    let res = num%100.0;

    let ans = (num/100.0).floor() + res/60.0;

    if orientation == "N" || orientation == "E" {
        return ans;
    } else {
        return -ans;
    }
}

fn extract_time(date: &str, time: &str) -> i64 {
    let mut dts = "".to_string();
    dts.push_str(date);
    dts.push_str(time.split('.').nth(0).unwrap());

    return Utc.datetime_from_str(&dts, "%d%m%y%H%M%S").unwrap().timestamp();
}

#[derive(Debug)]
pub struct Pos {
    pub lat    : f32,
    pub lon    : f32,
    pub device : i32,
    pub time   : i64,
    pub rcvd   : i64,
}

impl FromStr for Pos {
    type Err = String;

    fn from_str(s: &str) -> Result<Pos, Self::Err> {
        let pieces:Vec<&str> = s.split(',').collect();

        match pieces.get(0) {
            Some(piece) => if piece.ends_with("POS") {
                Ok(Pos{
                    lon    : extract_coord(pieces.get(6).unwrap().parse().unwrap(), pieces.get(7).unwrap()),
                    lat    : extract_coord(pieces.get(4).unwrap().parse().unwrap(), pieces.get(5).unwrap()),
                    device : pieces.get(1).unwrap().parse().unwrap(),
                    time   : extract_time(pieces.get(10).unwrap(), pieces.get(2).unwrap()),
                    rcvd   : get_time().sec,
                })
            } else {
                Err("No es una posición".to_string())
            },
            None => Err("Falló la conversión".to_string()),
        }
    }
}

pub struct NmeaCodec;

impl Decoder for NmeaCodec {
    type Item = Pos;
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<Pos>> {
        let index = buf.iter().position(|&b| b == b'$' || b == b'#');

        if index.is_none() {
            return Ok(None);
        }

        // consume to the next #
        let line = buf.split_to(index.unwrap());
        buf.split_to(1);

        let res:String = line.iter().map(|&b| b as char).collect();

        match res.parse() {
            Ok(pos) => {
                debug!("{}", res);
                buf.clear();

                Ok(Some(pos))
            },
            Err(e) => {
                error!("{:?}", e);
                Ok(None)
            },
        }
    }
}

impl Encoder for NmeaCodec {
    type Item = String;
    type Error = io::Error;

    fn encode(&mut self, msg: String, buf: &mut BytesMut) -> io::Result<()> {
        buf.extend(msg.as_bytes());
        Ok(())
    }
}

#[test]
fn test_parse_coords() {
    assert_eq!(extract_coord(2036.3883, "N"), 20.606472);
    assert_eq!(extract_coord(10323.9808, "W"), -103.39967);
    assert_eq!(extract_coord(2036.3883, "S"), -20.606472);
    assert_eq!(extract_coord(10323.9808, "E"), 103.39967);
}

#[test]
fn test_parse_multibyte_to_pos() {
    let pos:Pos = "\n$POS,1289,105127.000,A,2036.3883,N,10323.9808,W,0.0,1.7,130717,,,A/0000,0/0/93069700//f97/".parse().unwrap();

    assert_eq!(pos.lat, 20.6064716666666666);
    assert_eq!(pos.lon, -103.39967);
    assert_eq!(pos.device, 1289);
    assert_eq!(pos.rcvd, get_time().sec);
    assert_eq!(pos.time, 1499943087);

    let pos:Pos = "$POS,1289,105127.000,A,2036.3883,N,10323.9808,W,0.0,1.7,130717,,,A/0000,0/0/93069700//f97/".parse().unwrap();

    assert_eq!(pos.lat, 20.6064716666666666);
    assert_eq!(pos.lon, -103.39967);
    assert_eq!(pos.device, 1289);
    assert_eq!(pos.rcvd, get_time().sec);
    assert_eq!(pos.time, 1499943087);

    let pos:Pos = "POS,1289,105127.000,A,2036.3883,N,10323.9808,W,0.0,1.7,130717,,,A/0000,0/0/93069700//f97/".parse().unwrap();

    assert_eq!(pos.lat, 20.6064716666666666);
    assert_eq!(pos.lon, -103.39967);
    assert_eq!(pos.device, 1289);
    assert_eq!(pos.rcvd, get_time().sec);
    assert_eq!(pos.time, 1499943087);
}
