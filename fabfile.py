from __future__ import with_statement
from fabric.api import run, cd, env
from fabric.context_managers import prefix
from fabric.operations import prompt
import os

WEBAPPS_ROOT = '/home/fleety/webapps'

env.hosts = ['getfleety.com']
env.user  = 'fleety'

PROJECT_PATH = os.path.join(WEBAPPS_ROOT, 'gps.getfleety.com')

def deploy():
    with cd(PROJECT_PATH):
        run('git pull')
        run('cargo build')

    run('systemctl --user restart fleety-gps')
