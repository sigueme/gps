# Fleety GPS

Receives GPS data from various devices and reports to the rest of the platform.

## Development

You will need [rust](http://rustup.rs/)

### Run development server

```bash
$ cargo run
```

### How to build

```bash
$ cargo build --release
```

### How to test

```bash
$ cargo test
```
